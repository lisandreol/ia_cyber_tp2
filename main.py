import numpy as np
import tensorflow as tf
import random
import math
import sys
import os

from colorama import Fore, Style, init
from network import mynet
from data import mydata
from os import listdir

init()

EPOCHS = 4000
TEXT_SIZE = 16
KEY_SIZE = 16

optimizer = tf.keras.optimizers.Adam(learning_rate=0.1)

# DATASET 
data_set = mydata.get_dataset(2048,16,16)

# TensorBoard
current_path = os.getcwd() + "//logs"
try:
    for file in listdir(current_path):
        if file.endswith(".v2"):
            os.remove(current_path + "//" + file)
except:
    pass
summary_writer = tf.summary.create_file_writer("logs")
# Model
ALICE = mynet.build_generator(True)
BOB = mynet.build_generator(True)
EVE = mynet.build_generator(False)

def loss(input_message, out_message):
	# Mapping -1 to 0 and 1 to 1
	eve_diff = (out_message + 1.0)/2.0 - (input_message + 1.0)/2.0
  
	# Eve's average L1 distance Loss of the given batch
	return tf.reduce_sum(tf.abs(eve_diff))

# The basic loss function was: "((8 - eve_loss)^2)/64 + bob_loss"
# But we noticed that with a more weight of "bob_loss", the results were more often satisfying
def final_loss(eve_loss, bob_loss):
    return ((8 - eve_loss)**2)/64 + (bob_loss/16)**(1/4)

with summary_writer.as_default():
    for epoch in range(EPOCHS):
        x, y = data_set
        message = next(iter(x))
        key = next(iter(y))

        with tf.GradientTape(persistent=True) as tape:
            # Alice output
            out_alice = ALICE(np.array([np.concatenate((message, key))]))

            # Eve output
            out_eve = EVE(out_alice)
            loss_eve = loss(message, out_eve)

            # Bob output
            out_bob = BOB(np.array([np.concatenate((out_alice.numpy().flatten(),key))]))
            loss_bob = loss(message,out_bob)

            loss_final = final_loss(loss_eve, loss_bob)

        # Eve correction
        grads = tape.gradient(loss_eve, EVE.trainable_variables)
        optimizer.apply_gradients(zip(grads, EVE.trainable_variables))

        # Bob correction
        grads = tape.gradient(loss_bob, BOB.trainable_variables)
        optimizer.apply_gradients(zip(grads, BOB.trainable_variables))

        # Alice correction
        grads = tape.gradient(loss_final, ALICE.trainable_variables)
        optimizer.apply_gradients(zip(grads, ALICE.trainable_variables))

        tf.summary.scalar("loss_eve", loss_eve, step=epoch)
        tf.summary.scalar("loss_bob", loss_bob, step=epoch)
        # summary_writer.flush

        if epoch % 100 == 0:
            print (Fore.LIGHTWHITE_EX + "try number " + str(epoch) + ":  " + Fore.GREEN + "Bob Loss = " + Fore.LIGHTGREEN_EX + str(int(round(loss_bob.numpy()))) + " mistakes" + Fore.RED +" Eve Loss = " + Fore.LIGHTRED_EX + str(int(round(loss_eve.numpy()))) + " mistakes" + Style.RESET_ALL)
            '''
            print(out_eve)
            print(loss_eve)
            print(out_bob)
            print(loss_bob)
            '''
            print("final loss: ", loss_final)

print (Fore.LIGHTWHITE_EX + "try number " + str(EPOCHS) + ":  " + Fore.GREEN + "Bob Loss = " + Fore.LIGHTGREEN_EX + str(int(round(loss_bob.numpy()))) + " mistakes" + Fore.RED +" Eve Loss = " + Fore.LIGHTRED_EX + str(int(round(loss_eve.numpy()))) + " mistakes" + Style.RESET_ALL)
