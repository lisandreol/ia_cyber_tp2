import numpy as np

def random_bools(size, n):
  
  # create a batch size of 'size' with 'n' number of bits per sample
  temp =  np.random.random_integers(0, high=1, size=[size, n])
  
  # Convert 0 -> -1 and 1 -> 1 
  temp = temp*2 - 1
  
  return temp.astype(np.int32)
  
def get_dataset(sample_size, TEXT_SIZE, KEY_SIZE):

  m = random_bools(sample_size, TEXT_SIZE)
  k = random_bools(sample_size, KEY_SIZE)
  
  return m, k

# print(get_dataset(4096,16,16))