import tensorflow as tf
import numpy as np
from tensorflow.keras import layers

# Build a model with 1 FC layer and 4 Convolutionnal layers
def build_generator(key):
    model = tf.keras.Sequential()
    if key:
        # Input (32,1) : Alice or Bob
	    model.add(layers.Dense(input_shape=(32,),units=32, activation='relu'))
    else:
        # Input (16,1) : Eve
        model.add(layers.Dense(input_shape=(16,), units=32, activation='relu'))

    model.add(layers.Reshape((32, 1)))
    model.add(layers.Conv1D(filters=2, kernel_size=4,strides=1, activation='sigmoid',padding="same"))
    model.add(layers.Conv1D(filters=4, kernel_size=2,strides=2, activation='sigmoid',padding="valid"))
    model.add(layers.Conv1D(filters=4, kernel_size=1,strides=1, activation='sigmoid',padding="same"))
    model.add(layers.Conv1D(filters=1,kernel_size=1,strides=1, activation='tanh',padding="same"))
    # Convolutionnal return a 3D tensor. We need to flatten it to get a 2D one
    model.add(layers.Flatten())

    return model

# model_Alice = build_generator(True)
# out = np.array([np.concatenate(
# 	(np.array([-1,1,1,1,-1,-1,1,1,1,-1,-1,1,1,1,-1,1]),
# 	np.array([-1,1,1,1,-1,-1,1,1,1,1,-1,-1,-1,1,1,1])))])
# print(out.shape)
# model_out_Alice = model_Alice(out)
# print(model_out_Alice)

# model_Eve = build_generator(False)
# print(model_out_Alice)

# model_out_Eve = model_Eve(model_out_Alice)
# print(model_out_Eve)