import tensorflow as tf
import numpy as np
from sklearn.model_selection import train_test_split

EPOCHS = 1000

# mnist = tf.keras.datasets.mnist

def random_bools(size, n):
  
  # create a batch size of 'size' with 'n' number of bits per sample
  temp =  np.random.random_integers(0, high=1, size=[size, n])
  
  # Convert 0 -> -1 and 1 -> 1 
  temp = temp*2 - 1
  
  return temp.astype(np.float32)
  
def get_dataset(sample_size, TEXT_SIZE, KEY_SIZE):

  m = random_bools(sample_size, TEXT_SIZE)
  k = random_bools(sample_size, KEY_SIZE)
  
  return m, k

X, Y = get_dataset(5,28,28)

model = tf.keras.models.Sequential([
  tf.keras.layers.Flatten(input_shape=(28, 28)),
  tf.keras.layers.Dense(128, activation='relu'),
  tf.keras.layers.Dropout(0.2),
  tf.keras.layers.Dense(10, activation='softmax')
])

model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])


x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.2)

# for epochs in range(EPOCHS):


model.fit(x_train, y_train, epochs=5)

model.evaluate(x_test,  y_test, verbose=2)